## Background / User story

(Describe the motivation for making the change and refer to any further sources for context, where applicable)

## What to change

- **Design:** (Link to any designs here)
- **Research:** (Link to any research data here)
- **Spec:** ([Create spec merge request](https://gitlab.com/eyeo/specs/spec/merge_requests/new) and link to it from here)

## Hints for testers
(Mention any areas/scenarios that should be tested in addition to what's described in the spec)

## Hints for translators
(Mention any changes that were made to any files in the `locale` directory)

## Integration notes
(Mention any breaking changes and necessary changes that need to be made by any project that wants to include this change)

## Updates page (optional)

#TBA

(Should this issue be mentioned on the update page for the upcoming release? If yes [find](https://gitlab.com/eyeo/adblockplus/abpui/adblockplusui/issues?scope=all&utf8=%E2%9C%93&state=opened&search=%22update+page+for%22) (or create it if necessary) the update page issue, link it from here and add a reference to this issue to it. If not this section can be removed.)
